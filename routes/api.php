<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//routes below donot require token authentication
Route::get('users', 'ArticleController@index');
Route::patch('users', 'ArticleController@update');
Route::delete('users', 'ArticleController@delete');

Route::get('currentUser', 'ArticleController@show');

Route::get('products', function() {
 // If the Content-Type and Accept headers are set to 'application/json',
 // this will return a JSON structure. This will be cleaned up later.
 return Product::all();
});
Route::get('products/{id}', function($id) {
 return Product::find($id);
});
Route::post('products',  function(Request $request) {
 return Product::create($request->all);
});
Route::patch('products/{id}', function(Request $request, $id) {
 $products = Product::findOrFail($id);
 $products->update($request->all());
 return $products;
});
Route::delete('products/{id}', function($id) {
 Product::find($id)->delete();
 return 204;
});

Route::get('customers', function() {
 // If the Content-Type and Accept headers are set to 'application/json',
 // this will return a JSON structure. This will be cleaned up later.
 return customers::all();
});
Route::get('customers/{id}', function($id) {
 return customers::find($id);
});
Route::post('customers',  function(Request $request) {
 return customers::create($request->all);
});
Route::patch('customers/{id}', function(Request $request, $id) {
 $customers = customers::findOrFail($id);
 $customers->update($request->all());
 return $customers;
});
Route::delete('customers/{id}', function($id) {
 customers::find($id)->delete();
 return 204;
});

Route::get('invoices', function() {
 // If the Content-Type and Accept headers are set to 'application/json',
 // this will return a JSON structure. This will be cleaned up later.
 return invoices::all();
});
Route::get('invoices/{id}', function($id) {
 return invoices::find($id);
});
Route::post('invoices',  function(Request $request) {
 return invoices::create($request->all);
});
Route::patch('invoices/{id}', function(Request $request, $id) {
 $invoices = invoices::findOrFail($id);
 $invoices->update($request->all());
 return $invoices;
});
Route::delete('invoices/{id}', function($id) {
 invoices::find($id)->delete();
 return 204;
});

Route::get('test', function(){
	return response()->json(['foo'=>'bar']);
});
