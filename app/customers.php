<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class customers extends Model
{
    //fields to be created in the model for seeding
	protected $fillable = ['firstName', 'lastName' , 'email' , 'phone' , 'address'];
}
