<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //property to be mass assigned using Eloquents
    protected $fillable = ['name', 'price' , 'description' , 'service'];
}
