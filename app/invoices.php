<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class invoices extends Model
{
    //property to be mass assigned using Eloquents
	protected $fillable = ['date', 'customer','products'];
}
