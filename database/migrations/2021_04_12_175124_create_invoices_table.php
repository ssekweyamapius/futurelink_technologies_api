<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		//create a invoices table in the database
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
			$table->string('date');
			$table->string('customer');
			$table->string('products');
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		//stop invoices table creation if case of error
        Schema::dropIfExists('invoices');
    }
}
