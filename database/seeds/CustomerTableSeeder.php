<?php

use Illuminate\Database\Seeder;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
	public function run()
    {
		// Let's truncate our existing records to start from scratch.
		Customer::truncate();

		$faker = \Faker\Factory::create();

	   
		// generate a few  customers:
		for ($i = 0; $i < 20; $i++) {
			Customer::create([
				'firstName' => $faker->firstName,
				'lastName' => $faker->lastName,
				'email' => $faker->email,
				'phone' => $faker->phoneNumber,
				'address' => $faker->address,
				
			]);
		}
	}
}
