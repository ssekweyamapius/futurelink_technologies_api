<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       // Let's truncate our existing records to start from scratch.
        Product::truncate();

        $faker = \Faker\Factory::create();
        
       
        //generate data for our app:
        for ($i = 0; $i < 10; $i++) {
            Product::create([
                'name' => $faker->name,
                'price' => $faker->year,
                'description' => $faker->sentence,
                'service' => $faker->boolean:true,
            ]);
        }
    }
}
