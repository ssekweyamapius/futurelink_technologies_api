<?php

use Illuminate\Database\Seeder;

class InvoiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
	public function run()
    {
		// Let's truncate our existing records to start from scratch.
		Invoice::truncate();

		$faker = \Faker\Factory::create();

	 
		// create a few Invoices in our database:
		for ($i = 0; $i < 20; $i++) {
			Invoice::create([
				'date' => $faker->date,
				'customer' => $faker->name,
				'products' => $faker->sentence,
			]);
		}
	}
}
